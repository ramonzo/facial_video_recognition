from statistics import mode

import cv2
from keras.models import load_model
import numpy as np

from src.utils.datasets import get_labels
from src.utils.inference import detect_faces
from src.utils.inference import draw_text
from src.utils.inference import draw_bounding_box
from src.utils.inference import apply_offsets
from src.utils.inference import load_detection_model
from src.utils.preprocessor import preprocess_input

import json
import requests

# parameters for loading data and images
detection_model_path = '../trained_models/detection_models/haarcascade_frontalface_default.xml'
emotion_model_path = '../trained_models/emotion_models/fer2013_mini_XCEPTION.102-0.66.hdf5'
emotion_labels = get_labels('fer2013')

# hyper-parameters for bounding boxes shape
frame_window = 10
emotion_offsets = (20, 40)

# loading models
face_detection = load_detection_model(detection_model_path)
emotion_classifier = load_model(emotion_model_path, compile=False)

# getting input model shapes for inference
emotion_target_size = emotion_classifier.input_shape[1:3]

# starting lists for calculating modes
emotion_window = []

# starting video streaming
cv2.namedWindow('window_frame')
video_capture = cv2.VideoCapture(0)


def send_emotion(emotion, probability, emotion_label):
    # create a default json file to send
    url = "http://localhost/tensor/post_tensor_data.php"
    json_obj = {'emotion_id': str(emotion + 1), 'probability': str(probability), 'label': emotion_label}
    headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}
    # sending data to url
    response = requests.post(url, data=json.dumps(json_obj), headers=headers)
    # return result in url default type, in this case, json
    return response.text


i = 0
j = 0
old_label = 0
old_sum = 0
old_text = 0

while True:
    bgr_image = video_capture.read()[1]
    gray_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2GRAY)
    rgb_image = cv2.cvtColor(bgr_image, cv2.COLOR_BGR2RGB)
    faces = detect_faces(face_detection, gray_image)

    for face_coordinates in faces:

        x1, x2, y1, y2 = apply_offsets(face_coordinates, emotion_offsets)
        gray_face = gray_image[y1:y2, x1:x2]

        try:
            gray_face = cv2.resize(gray_face, emotion_target_size)
        except:
            continue

        gray_face = preprocess_input(gray_face, True)
        gray_face = np.expand_dims(gray_face, 0)
        gray_face = np.expand_dims(gray_face, -1)

        # model
        emotion_prediction = emotion_classifier.predict(gray_face)
        emotion_probability = np.max(emotion_prediction)
        emotion_label_arg = np.argmax(emotion_prediction)
        emotion_text = emotion_labels[emotion_label_arg]
        emotion_window.append(emotion_text)
        # end model

        if len(emotion_window) > frame_window:
            emotion_window.pop(0)
        try:
            emotion_mode = mode(emotion_window)
        except:
            continue

        # estimate of 12 frames per second
        if i >= 15:
            i = 0
            # if last emotion id is different of current
            if old_label != emotion_label_arg:
                # if exist or not more of two equal emotions probability summed
                if old_sum == 0:
                    r = send_emotion(emotion_label_arg, emotion_probability, emotion_text)
                else:
                    old_sum = old_sum/j
                    r = send_emotion(old_label, old_sum, old_text)
                    old_sum = 0
                # Print for requests messages
                print(r)

                old_label = emotion_label_arg
                old_text = emotion_text
                j = 0
            else:
                old_sum += emotion_probability
                j += 1

        color = emotion_probability * np.asarray((0, 255, 0))

        color = color.astype(int)
        color = color.tolist()

        draw_bounding_box(face_coordinates, rgb_image, color)
        draw_text(face_coordinates, rgb_image, emotion_mode,
                  color, 0, -45, 1, 1)

    bgr_image = cv2.cvtColor(rgb_image, cv2.COLOR_RGB2BGR)
    cv2.imshow('window_frame', bgr_image)

    i += 1

    # Press 'q' to break program
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
